import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

void main() => runApp(MyApp());
final List<String> titleEntries = <String>['A', 'B', 'C'];
final List<String> subtitleEntries = <String>['A', 'B', 'C'];
final int index = 0;

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profil Penerima',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        canvasColor: Colors.white,
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              fontSize: 14, 
              fontWeight: FontWeight.bold
            ),
            bodyText2: TextStyle(
              fontSize: 16, 
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            )),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Welcome to Flutter'),
        ),
        body: ListView(

          padding: const EdgeInsets.all(16.0),
          children: <Widget>[
            
            Text('PROFIL PENGGUNA', textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline6),
            const SizedBox(height: 12),

            TextButton.icon(
                icon: const Icon(
                        Icons.edit,
                        color: Colors.blue,
                        size: 16,
                      ),
                label: const Text("Ubah Data", style: TextStyle(fontSize: 16),),
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: Colors.blue,
                  padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                ),
            ),

            const SizedBox(height: 12),

            // Expanded(
            //   child: ListView.builder(
            //       itemCount: titleEntries.length,
            //       itemBuilder: (BuildContext context, int index) {
            //         return ListTile(
            //           title: Text('NIK', style: Theme.of(context).textTheme.bodyText1),
            //           subtitle: Text('123456789'),
            //         );
            //       }
            //     ),
            // ),

            ListTile(
              // leading: Icon(Icons.map),
              title: Text('NIK', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('123456789'),
            ),
            ListTile(
              // leading: Icon(Icons.photo_album),
              title: Text('Nama Lengkap', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Stefanus Ndaru Wedhatama'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Email', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('dev@gmail.com'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Alamat', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Jalan Merdeka Selatan, Jakarta Timur, Jakarta Barat'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Nomor Handphone', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('895062313'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Tanggal Lahir', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Nov. 18, 2021'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Jenis Kelamin', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Laki-Laki'),
            ),

            const SizedBox(height: 12),
            
            ElevatedButton.icon(
                icon: const Icon(
                        Icons.credit_card,
                        color: Colors.white,
                        size: 16,
                      ),
                label: const Text("Lihat Tiket Vaksin", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  onPrimary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                ),
            ),

            const SizedBox(height: 12),
            ElevatedButton.icon(
                icon: const Icon(
                        Icons.logout,
                        color: Colors.white,
                        size: 16,
                      ),
                label: const Text("Logout", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                onPressed: () {
                   print('Console Message Using Print');
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  onPrimary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                ),
            ),
          ],
        ),
      ),
    );
  }
}