# import form class from django
from django.utils.translation import gettext_lazy as _
from django import forms
from django.forms import widgets
from lab_2.models import Note

# https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django



class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields=['msg_to','msg_from','msg_title','msg_message']

        msg_to = forms.CharField(max_length='50', required=True)
        msg_from = forms.CharField(max_length='50', required=True)
        msg_title = forms.CharField(max_length='100', required=True)
        msg_message = forms.Textarea()

        labels = {
            'msg_to': _('Message Receiver (To)'),
            'msg_from': _('Message Sender (From)'),
            'msg_title': _('Message Title'),
            'msg_message': _('Content'),
        }

        widgets = {
            'msg_message': forms.Textarea(),
        }

        error_messages = {
		    'required' : 'Input cannot be empty',
	    }

