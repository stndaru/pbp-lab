from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'index_lab4.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'cards_lab4.html', response)

def navbar(request):
    return render(request, 'navbar.html')

def add_note(request):
  
    # create object of form
    form = NoteForm(request.POST or None)
      
    # check if form data is valid
    if (form.is_valid() and request.method == 'POST'):
        # save the form data to model
        form.save()
        # when saved go back to lab-3
        return HttpResponseRedirect('/lab-4')
    
    else:
        form = NoteForm()

    return render(request, 'form_lab4.html', {'form': form})