// @dart=2.12
import 'package:flutter/material.dart';
import 'package:date_field/date_field.dart';

void main() {
  runApp(MaterialApp(
    title: "PeduliLindungi 2.0 Profil Penerima Test",
    initialRoute: '/',
    routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const MainScreen(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/editProfil': (context) => const BelajarForm(),
      },
  ));
}

class BelajarForm extends StatefulWidget {
  const BelajarForm({Key? key}) : super(key: key);
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  late DateTime selectedData;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("BelajarFlutter.com"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: 12345678",
                      labelText: "NIK",
                      icon: const Icon(Icons.card_membership),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                          return 'NIK tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: Susilo Bambang",
                      labelText: "Nama Lengkap",
                      icon: const Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                          return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: example@mail.com",
                      labelText: "Alamat Email",
                      icon: const Icon(Icons.email),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                          return 'Email tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: Jalan Merdeka Selatan, Jakarta Pusat",
                      labelText: "Alamat",
                      icon: const Icon(Icons.streetview),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                          return 'Alamat tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DateTimeFormField(
                    decoration: const InputDecoration(
                      hintStyle: TextStyle(color: Colors.black45),
                      errorStyle: TextStyle(color: Colors.redAccent),
                      border: OutlineInputBorder(),
                      labelText: 'Tanggal Lahir',
                      icon: Icon(Icons.event_note),
                    ),
                    initialDate: DateTime.now(),
                    autovalidateMode: AutovalidateMode.always,
                    validator: (DateTime? e) =>
                        (e?.day ?? 0) == 1 ? 'Please not the first day' : null,
                    onDateSelected: (DateTime value) {
                      print(value);
                    },
                  ),
                ),
                ElevatedButton(
                  child: const Text(
                    "Simpan Data",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profil Penerima',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        canvasColor: Colors.white,
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              fontSize: 14, 
              fontWeight: FontWeight.bold
            ),
            bodyText2: TextStyle(
              fontSize: 16, 
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            )),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Welcome to Flutter'),
        ),
        body: ListView(

          padding: const EdgeInsets.all(16.0),
          children: <Widget>[
            
            Text('PROFIL PENGGUNA', textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline6),
            const SizedBox(height: 12),

            TextButton.icon(
                icon: const Icon(
                        Icons.edit,
                        color: Colors.blue,
                        size: 16,
                      ),
                label: const Text("Ubah Data", style: TextStyle(fontSize: 16),),
                onPressed: () {
                  Navigator.pushNamed(context, '/editProfil');
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: Colors.blue,
                  padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                ),
            ),

            const SizedBox(height: 12),

            // Expanded(
            //   child: ListView.builder(
            //       itemCount: titleEntries.length,
            //       itemBuilder: (BuildContext context, int index) {
            //         return ListTile(
            //           title: Text('NIK', style: Theme.of(context).textTheme.bodyText1),
            //           subtitle: Text('123456789'),
            //         );
            //       }
            //     ),
            // ),

            ListTile(
              // leading: Icon(Icons.map),
              title: Text('NIK', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('123456789'),
            ),
            ListTile(
              // leading: Icon(Icons.photo_album),
              title: Text('Nama Lengkap', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Stefanus Ndaru Wedhatama'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Email', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('dev@gmail.com'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Alamat', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Jalan Merdeka Selatan, Jakarta Timur, Jakarta Barat'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Nomor Handphone', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('895062313'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Tanggal Lahir', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Nov. 18, 2021'),
            ),
            ListTile(
              // leading: Icon(Icons.phone),
              title: Text('Jenis Kelamin', style: Theme.of(context).textTheme.bodyText1),
              subtitle: Text('Laki-Laki'),
            ),

            const SizedBox(height: 12),
            
            ElevatedButton.icon(
                icon: const Icon(
                        Icons.credit_card,
                        color: Colors.white,
                        size: 16,
                      ),
                label: const Text("Lihat Tiket Vaksin", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  onPrimary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                ),
            ),

            const SizedBox(height: 12),
            ElevatedButton.icon(
                icon: const Icon(
                        Icons.logout,
                        color: Colors.white,
                        size: 16,
                      ),
                label: const Text("Logout", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                onPressed: () {
                   print('Console Message Using Print');
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  onPrimary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                ),
            ),
          ],
        ),
      ),
    );
  }
}