# Jawaban Soal Lab 2
#### Stefanus Ndaru W - 2006526812 - PBP A
---


#### Apakah perbedaan antara JSON dan XML?

##### JSON
JSON atau JavaScript Object Notation merupakan suatu format data yang bisa diubah untuk disimpan atau dikirim dan terdiri dari atribut serta value atau arraynya.

##### XML
XML atau Extensible markup language merupakan markup language dari SGML berbasis teks yang mampu menyimpan dan mentransfer suatu data.

##### Perbedaan
Meskipun keduanya memiliki fungsi yang sama, yaitu sebagai media membawa data dari database ke website, keduanya memiliki beberapa perbedaan.
| JSON | XML |
| ------ | ------ |
| Didasari dari JavaScript | Didasari dari Standardised Generalized Markup Language (SGML) |
| Merupakan cara merepresentasikan objek dan tidak menggunakan tags | Merupakan markup language dan menggunakan tag untuk merepresentasikan struktur data |
| Bisa menyimpan array | Tidak bisa menyimpan array |
| Bisa diparse menggunakan function JavaScript standar | Harus diparse menggunakan parser XML |

##### Contoh Kode  
Kode JSON  
```json
{"employees":[
  { "firstName":"John", "lastName":"Doe" },
  { "firstName":"Anna", "lastName":"Smith" },
  { "firstName":"Peter", "lastName":"Jones" }
]}
```
Kode XML
```xml
<employees>
  <employee>
    <firstName>John</firstName> <lastName>Doe</lastName>
  </employee>
  <employee>
    <firstName>Anna</firstName> <lastName>Smith</lastName>
  </employee>
  <employee>
    <firstName>Peter</firstName> <lastName>Jones</lastName>
  </employee>
</employees>
```


---


#### Apakah perbedaan antara HTML dan XML?

##### HTML
HTML atau HyperText Markup Language merupakan markup language untuk dokumen yang ditujukan untuk ditampilkan dalam suatu web browser

##### XML
XML atau Extensible markup language merupakan markup language dari SGML berbasis teks yang mampu menyimpan dan mentransfer suatu data.

##### Perbedaan
Meskipun keduanya memiliki bahasa yang sama yaitu markup language, keduanya memiliki perbedaan, dan yang paling utama adalah fungsinya. HTML digunakan untuk menampilkan tampilan dari suatu web, sementara XML digunakan untuk membawa data. Selain itu, terdapat beberapa perbedaan tambahan.
| HTML | XML |
| ------ | ------ |
| Bersifat statis | Bersifat dinamis |
| Tidak case sensitive | Case sensitive |
| Tagsnya sudah didefinisikan dan tidak bisa membuat sembarang tags baru | Tags bisa didefinisikan secara bebas oleh penulis |
| Tags digunakan untuk menampilkan data | Tags digunakan untuk menyimpan data |
| Boleh tidak menggunakan tag penutup | Harus menggunakan tag penutup |

##### Contoh Kode  
Kode HTML   
```html
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>

<h1>This is a Heading</h1>
<p>This is a paragraph.</p>

</body>
</html>
```
Kode XML
```xml
<employees>
  <employee>
    <firstName>John</firstName> <lastName>Doe</lastName>
  </employee>
  <employee>
    <firstName>Anna</firstName> <lastName>Smith</lastName>
  </employee>
  <employee>
    <firstName>Peter</firstName> <lastName>Jones</lastName>
  </employee>
</employees>
```

---
## Sumber Referensi  
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://medium.com/@oazzat19/what-is-the-difference-between-html-vs-xml-vs-json-254864972bbb
https://www.w3schools.com/js/js_json_xml.asp
https://www.geeksforgeeks.org/html-vs-xml/
