from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from django.core import serializers
from .models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'index_lab2_custom.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

def jsonResponse(request):
    response = {
        'id' : 1
    }
    return JsonResponse(response)

def jsontest(request):
    return render(request, 'jsontest.html')