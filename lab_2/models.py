from django.db import models

# Create your models here.
class Note(models.Model):
    msg_to = models.CharField(max_length=50)
    msg_from = models.CharField(max_length=50)
    msg_title = models.CharField(max_length=100)
    msg_message = models.TextField()
