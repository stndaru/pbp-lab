from django.db import models
from datetime import datetime, date

# Create your models here.
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    dob = models.DateField()