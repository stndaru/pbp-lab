from django.http.response import Http404
from django.shortcuts import (render, get_object_or_404)
from django.http import (HttpResponse,HttpResponseRedirect)
from .models import Message, Person
from .forms import MessageForm

# Create your views here.
def index(request):
    person = Person.objects.all().values()
    response = {'persons': person}
    return render(request, 'index.html', response)

def search(request):        
    if request.method == 'GET': # this will be GET now      
        person_npm =  request.GET.get('search')      
        try:
            person = Person.objects.get(npm__icontains=person_npm) # filter returns a list so you might consider skip except part
            message = Message.objects.filter(sender = person)
        except Person.DoesNotExist:
            return HttpResponse(status=404)
        
        return render(request,"search.html",{"person":person, "messages":message})
    else:
        return render(request,"search.html",{})

def viewDetails(request, npm):

    # try except ini bisa diganti jadi get get_object_or_404 kalo mau langsung 404
    # tapi kalo mau custom exception kek redirect ke page, enakan try except
    try:
        person = Person.objects.get(npm = npm)
        message = Message.objects.filter(sender = person)

    except Person.DoesNotExist:
        return HttpResponse(status=404)

    return render(request,"search.html", {"person":person, "messages":message})

def delete_view(request, id):
    # dictionary for initial data with
    # field names as keys
 
    # fetch the object related to passed id
    msgObj = get_object_or_404(Message, id = id)
    personObj = get_object_or_404(Person, npm = msgObj.sender.npm)
 
 
    if request.method =="POST":
        # delete object
        msgObj.delete()
        # after deleting redirect to
        # home page
        return HttpResponseRedirect("/test-tk")
 
    return render(request, "delete_view.html", {"message":msgObj, "person":personObj})

def create_view(request, npm):

    personObj= get_object_or_404(Person, npm = npm)
    form = MessageForm(request.POST or None)

    if (form.is_valid() and request.method == 'POST'):
        # save the form data to model
        obj = form.save(commit=False)
        obj.sender = Person.objects.get(npm = npm)
        obj.save()
        # when saved go back to lab-3
        return HttpResponseRedirect('/test-tk')
    
    else:
        form = MessageForm()

    return render(request, "form.html", {"form":form, "person":personObj})