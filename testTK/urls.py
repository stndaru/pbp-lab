from django.urls import include, path
from .views import index, search, viewDetails, delete_view, create_view

urlpatterns = [
    path('', index, name='index'),
    path('search', search, name='search'),
    path('<npm>', viewDetails, name='viewDetails'),
    path('<id>/delete', delete_view ),
    path('<npm>/create', create_view),
]