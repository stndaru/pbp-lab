from django.db import models

# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10, primary_key=True)

class Message(models.Model):
    msg_title = models.CharField(max_length=100)
    msg_message = models.TextField()
    sender = models.ForeignKey(Person, db_column="Person.npm", on_delete=models.CASCADE)
