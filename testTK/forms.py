# import form class from django
from django.utils.translation import gettext_lazy as _
from django import forms
from django.forms import widgets
from testTK.models import Message, Person

# https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django



class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields=['msg_title','msg_message']

        msg_title = forms.CharField(max_length='100', required=True)
        msg_message = forms.Textarea()

        labels = {
            'msg_title': _('Message Title'),
            'msg_message': _('Content'),
        }

        widgets = {
            'msg_message': forms.Textarea(),
        }

        error_messages = {
		    'required' : 'Input cannot be empty',
	    }

