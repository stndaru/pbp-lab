from django.apps import AppConfig


class TesttkConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'testTK'
