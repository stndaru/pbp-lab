from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import (render, get_object_or_404)
from lab_2.models import Note
from lab_5.forms import NoteForm
from django.core import serializers
import json

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'index_lab5.html', response)

def get_note(request, id):
    note = get_object_or_404(Note, id = id)
    return render(request, 'modal.html', {"note":note})

def edit_note(request, id):

    note = get_object_or_404(Note, id = id)
  
    # create object of form
    form = NoteForm(request.POST or None, instance=note)
      
    # check if form data is valid
    if (form.is_valid() and request.method == 'POST'):
        # save the form data to model
        print("SAVED")
        obj = form.save(commit=False)
        print(obj)
        obj.save()
        return HttpResponse(json.dumps([]), content_type="application/json")

    return render(request, 'edit_note.html', {'form':form, 'note':note})

def getSpecific(request, id):
    data = serializers.serialize('json', Note.objects.filter(id = id))
    return HttpResponse(data, content_type="application/json")

# def bound_form(request, id): 
#     item = Item.objects.get(id=id) 
#     form = ItemForm(initial={'name': item.name}) 
#     return render_to_response('bounded_form.html', {'form': form}) 