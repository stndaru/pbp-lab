from django.urls import include, path
from .views import index, get_note, edit_note, getSpecific

urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>', get_note),
    path('edit/<id>', edit_note),
    path('get/<id>', getSpecific),
]